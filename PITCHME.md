## Bureau of Justice Statistics
 Research and Discussion    
### Census of Federal Law Enforcement Officers
---
@snap[north span-100]
## **Purpose**
@snapend

@snap[mipoint]
### *Obtain a Census of Federal Law Enforcement Agencies*
@snapend
---

@snap[north span-100]
@box[bg-blue text-white rounded span-100](Population # Officers with federal arrest authority )
@snapend

@snap[midpoint span-100]
@box[ text-white rounded fragment](Officers working in criminal investigation/law enforcement, police patrol/response, security/protection, court operations, and corrections, by agency and state)
@snapend

@snap[south-west span-30]
@box[bg-green text-white rounded fragment](Authorized # Duty Firearms)
@snapend

@snap[south-east span-30]
@box[bg-green text-white rounded fragment](Collection Method # Census form via Email or Fax)
@snapend

@snap[south span-30]
@box[bg-green text-white rounded fragment ](Excluding # Armed Forces, CIA, TSA)
@snapend




---

@snap[north span-55]
### No Ethical concerns.
@snapend

@snap[midpoint span-55 fragment]
It is appropriate for the Government to have a count of employees.
@snapend


@snap[south span-100]
## Ethics
@snapend

---
@snap[east span-60]
As long as none of the data includes the names and locations of law enforcement officers, privacy isn't a concern.
@snapend

@snap[north-west span-40]
<br><br><br>
![IMAGE](assets/img/form.png)
@snapend

@snap[south span-100]
## Privacy
@snapend



---

@snap[south span-100]
## Summary / Status
@snapend

@snap[midpoint span-65 fragment]
The 2016 data includes counts from 83 Federal Agencies
@snapend

@snap[north-east span-30]
@box[text-white rounded fragment](Years Available # **1996 - 2016** (Biennial))
@snapend

@snap[north-west span-35 fragment]
@box[text-white](Collection Status #  **ACTIVE**)
@snapend



---
@snap[south span-100]
### **Interesting Statistics from 2016**
@snapend

@snap[north-west span-35]
@box[text-white rounded fragment](**47%** of all officers belong to Department of Homeland Security. (Brooks, 2019))
@snapend

@snap[north span-35]
@box[text-white rounded fragment](Bureau of Indian Affairs had highest rate of assaults on officers, **143 per 100**.(Brooks, 2019) )
@snapend

@snap[north-east span-30]
@box[text-white rounded fragment](Amtrak Police saw the largest increase in full time officers at **40%**.(Brooks,2019) )
@snapend


@snap[midpoint span-100]
<br><br>
@box[text-white rounded fragment]( At the end of 2016 the United States had **132,110** full time federal law enforcement officers. (Brooks, 2019)  )
@snapend

---
@box[text-white rounded]( **Closing Impressions** # Any type of Census will be a rich set of data. However, I am surprised that this sort of information isn't readily available to the Federal Government at all times. How is it that we don't at all times know how many Federal Law Enforcement officers we have? This data set seems to be an effort to rectify that problem. )

---
@snap[north span-100]
## References
@snapend

@snap[midpoint span-100]
Brooks, C. (2019). Federal Law Enforcement Officers, 2016 Statistical Tables. Statistical Tables, 12.
@snapend
